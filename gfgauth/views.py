import httplib2
from rest_framework.decorators import api_view
from googleapiclient.discovery import build
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from .models import CredentialsModel,User
from gfglogin import settings
from oauth2client.contrib import xsrfutil
from oauth2client.client import flow_from_clientsecrets
from oauth2client.contrib.django_util.storage import DjangoORMStorage
from django.shortcuts import render
from httplib2 import Http
import requests
from .serializer import UserSerializer
from django.core.paginator import Paginator
from rest_framework.response import Response

def home(request):
    status = True

    #delete user objects
    for item in User.objects.all():
        item.delete()

    if not request.user.is_authenticated:
        return HttpResponseRedirect('admin')

    storage = DjangoORMStorage(CredentialsModel, 'id', request.user, 'credential')
    credential = storage.get()
    try:
        access_token = credential.access_token
        resp, cont = Http().request("https://www.googleapis.com/auth/gmail.readonly",
                                    headers={'Host': 'www.googleapis.com',
                                            'Authorization': access_token})

        # status = False
    except:
        status = False
        print('Not Found')
    if status:
        res = requests.get('https://www.googleapis.com/gmail/v1/users/me/threads',headers={'Authorization': 'Bearer '+access_token})
        thread_list = res.json()
        if 'threads' in thread_list.keys():
            thread_list = thread_list['threads']
            data={}
            count = 0
            for entry in thread_list:
                count = count+1
                data['MessageID'] = count
                data['Message'] = entry['snippet']
                serializer_user = UserSerializer(data=data)
                if serializer_user.is_valid():
                    serializer_user.save()
                else:
                    print(serializer_user.errors)

            qs_user = User.objects.all()
            paginator = Paginator(qs_user, 10)
            users = paginator.page(1)
            print(access_token)
            return render(request, 'email_list.html', {'users': users})
        else:
            status = False
    return render(request, 'index.html', {'status': status})


@api_view(['GET'])
def email_list(request):
    if request.method == 'GET':
        page = request.query_params.get('page')
        qs_user = User.objects.all()
        paginator = Paginator(qs_user, 10)
        users = paginator.page(page)
        
        return render(request, 'email_list.html', {'users': users})
        
################################
#   GMAIL API IMPLEMENTATION   #
################################

# CLIENT_SECRETS, name of a file containing the OAuth 2.0 information for this
# application, including client_id and client_secret, which are found
# on the API Access tab on the Google APIs
# Console <http://code.google.com/apis/console>


FLOW = flow_from_clientsecrets(
    settings.GOOGLE_OAUTH2_CLIENT_SECRETS_JSON,
    scope='https://mail.google.com/',
    redirect_uri='http://127.0.0.1:8000/oauth2callback',
    prompt='consent')


def gmail_authenticate(request):
    storage = DjangoORMStorage(CredentialsModel, 'id', request.user, 'credential')
    credential = storage.get()
    credential = None
    if credential is None or credential.invalid:
        FLOW.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY,
                                                       request.user)
        authorize_url = FLOW.step1_get_authorize_url()
        return HttpResponseRedirect(authorize_url)
    else:
        http = httplib2.Http()
        http = credential.authorize(http)
        service = build('gmail', 'v1', http=http)
        print('access_token = ', credential.access_token)
        status = True
        print("here")
        return render(request, 'index.html', {'status': status})


def auth_return(request):
    get_state = bytes(request.GET.get('state'), 'utf8')
    if not xsrfutil.validate_token(settings.SECRET_KEY, get_state,
                                   request.user):
        return HttpResponseBadRequest()
    credential = FLOW.step2_exchange(request.GET.get('code'))
    storage = DjangoORMStorage(CredentialsModel, 'id', request.user, 'credential')
    storage.put(credential)
    print("access_token: %s" % credential.access_token)
    return HttpResponseRedirect("/")